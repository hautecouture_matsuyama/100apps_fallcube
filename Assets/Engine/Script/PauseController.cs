﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PauseController : MonoBehaviour {

	public Sprite pauseSprite,unpauseSprite; //Sprite of button
	public Button pauseBtn; //pause button
	private bool isPaused; //for check pause or not pause

	public static PauseController instance;
	
	void Awake(){
		instance = this;
	}
	
	public void Pause(){
		if(!isPaused){	
				isPaused = true;
				GameController.instance.Paused(); //Show Paused Canvas
				pauseBtn.GetComponent<Image>().sprite = pauseSprite; //Change sprite
				Time.timeScale = 0; //Change timeScale to 0
			
			}else{
				isPaused = false;
				GameController.instance.Paused(); //Hide Paused Canvas
				pauseBtn.GetComponent<Image>().sprite = unpauseSprite; //Change sprite
				Time.timeScale = 1;	 //Change timeScale to 1
		}
	}
	
	public bool IsPaused(){
		return isPaused;
	}
}
