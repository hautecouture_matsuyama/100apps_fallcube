﻿using UnityEngine;
using System.Collections;

public class Coin : MonoBehaviour
{

    float speed; //Speed of the tile
    public float speedOffset;
    public int life = 1;
    
    // Use this for initialization
    public Rigidbody2D rigid;

   

    void Awake()
    {

       

        speed = speedOffset / (1 + life / 3);
        rigid.velocity = Vector3.down * speed; //Move tiles


    }

    void OnTriggerEnter2D(Collider2D coll)
    {

        if (coll.tag == "Ball" || coll.tag == "NoDamageBall")
        {
            if (ScoreHandler.instance.GetRewardGame())
            {
                ScoreHandler.instance.increaseSpecialPoints(2);
            }
            else
            {
                ScoreHandler.instance.increaseSpecialPoints(1);
            }
            destr();
        }

    }

    public GameObject destructionParticle;

    void destr() {
        Instantiate(destructionParticle, transform.position, Quaternion.identity);
        Destroy(gameObject);

    }
}
