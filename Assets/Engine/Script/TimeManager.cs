﻿using UnityEngine;
using System.Collections;

public class TimeManager : MonoBehaviour
{


    public static TimeManager instance;

    void Awake()
    {
        instance = this;
    }

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }





    public void startSlowMotion()
    {
        StartCoroutine(slowMotionCoroutine());
    }

    public IEnumerator slowMotionCoroutine()
    {


        float minTimescale = 0.5f;
        float maxTimescale = 1;

        float fadingTime = 1f;
        float bonusActivate = 5;

        float elapsedTime = 0f;


        
        while (elapsedTime < fadingTime)
        {
            elapsedTime += Time.deltaTime/Time.timeScale;
            Time.timeScale = Mathf.Lerp(maxTimescale, minTimescale, elapsedTime / fadingTime);
       //     SoundManager.instance.backgroundMusic.pitch = Time.timeScale;    
            yield return new WaitForEndOfFrame();
        }
        
       
        yield return new WaitForSeconds(Time.timeScale/minTimescale);
    

        
        elapsedTime = 0;

        while (elapsedTime < fadingTime)
        {
            elapsedTime += Time.deltaTime / Time.timeScale;
            Time.timeScale = Mathf.Lerp(minTimescale, maxTimescale, elapsedTime / fadingTime);
          //  SoundManager.instance.backgroundMusic.pitch = Time.timeScale;
            yield return new WaitForEndOfFrame();
        }
        

        yield return null;

    }


}
