﻿using UnityEngine;
using System.Collections;



public class SoundManager : MonoBehaviour {

    public static SoundManager instance;


    public AudioSource gameOverSound;
    public AudioSource menuButtonSound;



    void Awake()
    {
        instance = this;
    }

   public void PlayAudioSource(AudioSource audioSource)
    {
        if (Bot.instance.botEnable) { return; }
        audioSource.pitch = Time.timeScale;
        audioSource.Play();
    }


    public void PlayGameOverSound()
    {
        PlayAudioSource(gameOverSound);
    }

    public void PlayMenuButtonSound() {

    menuButtonSound.Play();
    }
    
}
