﻿using UnityEngine;
using System.Collections;

public class DestroyByBoundary : MonoBehaviour
{

    void OnTriggerExit2D(Collider2D coll)
    {

        if (coll.tag == "Tile" || coll.tag == "Bomb")
        {
            if (!Bot.instance.botEnable)
            {
                TileSpawner.instance.EndCubeParticre(coll.transform);
                Player.instance.TakeDamage(1, coll.gameObject); //When the tile hit boundary you're dead
            }

         //   Destroy(coll.gameObject); //Destroy the tiles
        }

        if (coll.tag == "Ball")
        {
            if (!Bot.instance.botEnable)
            {
                TileSpawner.instance.EndBallParticle(coll.gameObject);
                Player.instance.TakeDamage(1,coll.gameObject); //When the ball hits the boundary It means a miss shoot.


              //  InGameCanvas.instance.showOuchText();
            }
          //  Destroy(coll.gameObject); //Destroy the ball
        }

        if (coll.tag == "NoDamageBall")
        {
            Destroy(coll.gameObject);
        }

        if (coll.tag == "Coin")
        {
            Destroy(coll.gameObject);
        }

        if (coll.tag == "Heart")
        {
            Destroy(coll.gameObject);
        }

        if (coll.tag == "Clesidra")
        {
            Destroy(coll.gameObject);
        }

    }
}
