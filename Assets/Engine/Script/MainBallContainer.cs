﻿using UnityEngine;
using System.Collections;

public class MainBallContainer : MonoBehaviour
{

    public Ball childBall;

    // Use this for initialization
    void Start()
    {
        childBall = GetBallChild();
    }

    // Update is called once per frame
    void Update()
    {
        if (ShopHandler.instance.shopItemToUse.name != childBall.name)
        {
            childBall.gameObject.SetActive(false);
            Destroy(childBall.transform.gameObject);
            GameObject newBall = (GameObject)Instantiate(ShopHandler.instance.shopItemToUse.gameObject);
            newBall.name = ShopHandler.instance.shopItemToUse.name;
            newBall.transform.parent = transform;
            newBall.transform.localPosition = new Vector3(0, 0, 0);
            newBall.transform.localScale = ShopHandler.instance.shopItemToUse.originalScale;
            childBall = GetBallChild();
        }
    }

    public Ball GetBallChild()
    {
        return GetComponentInChildren<Ball>();
    }

}
