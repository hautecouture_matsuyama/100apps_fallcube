﻿using UnityEngine;
using System.Collections;

public class Clesidra : MonoBehaviour
{

    float speed; //Speed of the tile
    public float speedOffset;

    // Use this for initialization
    public Rigidbody2D rigid;

    public int life = 1;

    void Awake()
    {
        speed = speedOffset;
        rigid.velocity = Vector3.down * speed; //Move tiles
    }

    void OnTriggerEnter2D(Collider2D coll)
    {
        string collTag = coll.tag;

        if (coll.tag == "Ball" || coll.tag == "NoDamageBall")
        {
            TimeManager.instance.startSlowMotion();
            InGameCanvas.instance.showTimeWarpText();
            TileSpawner.instance.ClockGet(transform);
            Destroy(gameObject);
        }


    }

}
