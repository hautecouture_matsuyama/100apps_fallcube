﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;

public class Player : MonoBehaviour
{


    public bool isDead;
    public int health; //Number of ball
    int startHealth;
    public AudioClip gameOverAudio; //Sound of game over
    public static Player instance;

    private int fingerId = -1; //For every non-mobile Platform


    // Use this for initialization
    void Awake()
    {
        if (Application.isMobilePlatform)
        {
            fingerId = 0; //for mobile and unity
        }

        instance = this;
        isDead = false;
    }

    void Start()
    {
        startHealth = health;
    }

    // Update is called once per frame
    void Update()
    {

        if (health <= 0 && !isDead)
        {
            Dead(); //Call method Dead
        }
        else
        {

            HandleInput();
        }
    }

    public void HandleInput()
    {
        if (!Application.isMobilePlatform)
        {
            if (Input.GetMouseButtonDown(0) && !isDead && !PauseController.instance.IsPaused())
            {
                //For Block click through UI 
                if (EventSystem.current.IsPointerOverGameObject(fingerId))
                {
                    return; //Not Shoot the ball
                }

                if (GameController.instance.gameState != GameController.GameState.game) return;

                Vector3 mousePos = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x,Input.mousePosition.y,Camera.main.WorldToScreenPoint(Paddle.instance.transform.position).z));
                Paddle.instance.Shooting(mousePos); //Shoot the ball
            }
        }
        else
        {
            if (Input.touchCount > 0 && !isDead && !PauseController.instance.IsPaused())
                foreach (Touch touch in Input.touches)
                {
                    if (touch.phase == TouchPhase.Began)
                    {
                        if (EventSystem.current.IsPointerOverGameObject(fingerId))
                        {
                            return; //Not Shoot the ball
                        }

						Vector3 inputTouch = Camera.main.ScreenToWorldPoint(new Vector3(touch.position.x,touch.position.y,Camera.main.WorldToScreenPoint(Paddle.instance.transform.position).z));
						Paddle.instance.Shooting(inputTouch);
                    }
                }
        }
    }



    public void TakeDamage(int amount,GameObject ball)
    {
        //if (health > 0)
        //{
        //   health -= amount; //Decrease health
        //  InGameCanvas.instance.shakeFX.Shake();
        //}
        GameOverManager.instance.GameOverEvent(ball);

    }

    public void Dead()
    {
        isDead = true;  
   GameController.instance.GameOver(); //Call Game Over
    }


    public void increaseLife()
    {
        health++;
        InGameCanvas.instance.shakeFX.Shake();
        InGameCanvas.instance.heartDropFX.startEffect();
    }

    public void reset()
    {
        isDead = false;
        health = startHealth;
    }


}
