﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using NendUnityPlugin.AD;

public class GameController : MonoBehaviour
{

    public static GameController instance;

    public Canvas inGameCanvas; //Canvas for show when play	
    public Canvas pausedCanvas; //Canvas for show when game paused
    public Canvas mainMenuCanvas; // Canvas for the main menu

    public enum GameMode { CLASSIC, TIMEATTACK, SPAWNATTACK, NUMBERATTACK }
    public GameMode gameMode;

    public enum GameState { menu, game, gameover, shop }
    public GameState gameState;
    public bool oneMoreChanceUsed = false;

    [SerializeField]
    private NendAdBanner nendBanner;

    [SerializeField]
    private GameObject hcBanner;

    void Awake()
    {
        Application.targetFrameRate = 60;
        instance = this;
        gameState = GameState.menu;
    }

	void Start(){
//		if (CrossPromoter.instance.initialized && !CrossPromoter.instance.error) {
	//		CrossPromoter.instance.Show ();
	//	}
	}



    public void RestartEntireGame()
    {
        oneMoreChanceUsed = false;
        if (TileSpawner.instance != null) { Destroy(TileSpawner.instance.gameObject); } // Destroying all the tiles on the screen;
        if (BulletsContainer.instance != null) { Destroy(BulletsContainer.instance.gameObject); } // Destroying all the bullets on the screen;
        if (TileLifeHandler.instance != null) { Destroy(TileLifeHandler.instance.gameObject); }                                                                              // if(Paddle.instance != null) { Destroy(Paddle.instance.gameObject); }   

        Player.instance.reset();
        ScoreHandler.instance.reset();

        inGameCanvas.enabled = true; //Hide Canvas of ingame states

        GameObject newTileSpawner = (GameObject)Instantiate(Resources.Load("TileSpawner"), Vector3.zero, Quaternion.identity); //Instantiating the new spawner
        GameObject newBulletContainer = (GameObject)Instantiate(Resources.Load("BulletsContainer"), Vector3.zero, Quaternion.identity); // Instantiating the new bullet container
        GameObject newTileLifeHandler = (GameObject)Instantiate(Resources.Load("TileLifeHandler"), Vector3.zero, Quaternion.identity); // Instantiating the new bullet container
    }


    public void StartGame()
    {
        Bot.instance.botEnable = false;
        GUIManager.instance.inGameGUI.gameObject.SetActive(true);
        //GUIManager.instance.tutorialGUI.ShowIfNeverAppeared();
        TileSpawner.instance.ExplodeAllSpawnedObjects();
        RestartEntireGame();
        gameState = GameState.game;
		//AdNetworks.instance.ShowBanner ();

    }




    public void GameOver()
    {
        //AdNetworks.instance.HideBanner ();
        //AdNetworks.instance.ShowInterstitial ();

        nendBanner.Show();
        hcBanner.SetActive(true);

        int random = Random.Range(0, 100);
        if(random <= 33)
        {
            NendAdInterstitial.Instance.Show();
        }

        SoundManager.instance.PlayGameOverSound();
        gameState = GameState.gameover;
        /*   if (oneMoreChanceUsed)
           {
               Destroy(TileSpawner.instance.gameObject);
               Leaderboard.instance.reportScore(ScoreHandler.instance.score);
               GUIManager.instance.ShowGameOverGUI();
               GUIManager.instance.inGameGUI.gameObject.SetActive(false);
           }
           else
           {
               GUIManager.instance.ShowOneMoreChanceGUI();
           } */
        Destroy(TileSpawner.instance.gameObject);
        //Leaderboard.instance.reportScore(ScoreHandler.instance.score);
        GUIManager.instance.ShowGameOverGUI();
      //  GUIManager.instance.inGameGUI.gameObject.SetActive(false);
    }


    public void Paused()
    {
        pausedCanvas.enabled = !pausedCanvas.enabled; //Show or Hide paused canvas
    }


}
