﻿using UnityEngine;
using System.Collections;
public class BombEffect : MonoBehaviour {

    public Color firstColor;
    public Color secondColor;
    public SpriteRenderer sprite;
    public TextMesh text; 

 public   float switchTime = 0.05f;
    float originalSwitchTime;
    bool switchColor;
    void Awake() {
        originalSwitchTime = switchTime;
        SwitchColor();

    }
    void Update() {
        if (switchTime > 0) {
            switchTime -= Time.deltaTime;
            if (switchTime <= 0)
            {

                SwitchColor();
                switchTime = originalSwitchTime;

            }

        }





    }


    void SwitchColor() {


        switchColor = !switchColor;

        if (switchColor) {
            sprite.color = firstColor;
        } else { sprite.color = secondColor; }

        text.color = sprite.color;

    }
















}
