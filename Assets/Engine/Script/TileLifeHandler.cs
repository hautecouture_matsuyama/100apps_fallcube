﻿using UnityEngine;
using System.Collections;

public class TileLifeHandler : MonoBehaviour
{

    public int CurrentMaxHealth;
    public int minHealth = 3;
    public int maxHealth = 6;

    public static TileLifeHandler instance;


    public float elapsedSecondsToIncreaseMaxLife;
    public float SecondsToIncreaseMaxLife = 5f;

    void Awake()
    {
        instance = this;
    }

    void Start()
    {
        ResetMaxHealth();
    }

    void Update()
    {
        //INCREASING THE MAXIMIUM LIFE OF THE TILE OVER TIME
        if (elapsedSecondsToIncreaseMaxLife < SecondsToIncreaseMaxLife)
        {
            elapsedSecondsToIncreaseMaxLife += Time.deltaTime;
            if (elapsedSecondsToIncreaseMaxLife >= SecondsToIncreaseMaxLife)
            {
                elapsedSecondsToIncreaseMaxLife = 0;
                IncrementCurrentMaxHealth();
            }
        }
    }

    public void IncrementCurrentMaxHealth()
    {

        CurrentMaxHealth++;
        CurrentMaxHealth = Mathf.Clamp(CurrentMaxHealth, minHealth, maxHealth);

    }

    public void ResetMaxHealth()
    {
        CurrentMaxHealth = minHealth;
    }


}

