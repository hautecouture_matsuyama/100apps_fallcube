﻿using UnityEngine;
using System.Collections;

public class HitParticle : MonoBehaviour {

	public float time; //Specific Time for destroy
    public AudioSource audioSource;

	// Use this for initialization
	void Start () {

        audioSource.pitch = Random.Range(0.8f, 1.2f);
        audioSource.Play();

        Destroy (gameObject, time); //Destroy the object after specific time
	}

    void RandomizePitch()
    {

    }
	

}
