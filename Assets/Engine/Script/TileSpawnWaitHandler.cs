﻿using UnityEngine;
using System.Collections;

public class TileSpawnWaitHandler : MonoBehaviour {

    public static TileSpawnWaitHandler instance;

    public float firstTileSpawnWait = 1;

    public float spawnWaitOffset = 0;
    float spawnWaitDecrement = 0.1f;

    public float secondsToDecreaseSpawnWait = 5f;

    public float maxMinSpawnWait;

    public float spawnWaitWhenBotEnabled;

    public float secondsToReachMaxMinSpawnWaitOffset = 20;

    public void Awake()
    {
        instance = this;
    }

    // Use this for initialization
    void Start()
    {
        spawnWaitDecrement = ((spawnWaitOffset - maxMinSpawnWait) / secondsToReachMaxMinSpawnWaitOffset) * secondsToDecreaseSpawnWait;
        StartCoroutine(IncreaseSpeedOverTime());
    }

    // Update is called once per frame
    void Update()
    {

    }


    public IEnumerator IncreaseSpeedOverTime()
    {
        while (true)
        {
            yield return new WaitForSeconds(secondsToDecreaseSpawnWait);
            spawnWaitOffset -= spawnWaitDecrement;

            if(spawnWaitOffset < maxMinSpawnWait && GameController.instance.gameMode != GameController.GameMode.SPAWNATTACK)
            {
                spawnWaitOffset = maxMinSpawnWait;
            }

            if (Bot.instance.botEnable)
            {
                spawnWaitOffset = spawnWaitWhenBotEnabled;
            }

        }
    }
}
