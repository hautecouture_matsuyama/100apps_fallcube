﻿using UnityEngine;
using System.Collections;

public class Bot : MonoBehaviour
{


    public bool botEnable;
    public static Bot instance;


    void Start()
    {
        StartBot();
    }

    void Awake()
    {
        instance = this;
       

    }

    public void ShootTile()
    {
        if (botEnable)
        {

            Tile tile = TileSpawner.instance.lastSpawnedTile.GetComponent<Tile>();
           // if(tile.life >= 2) { tile.life = 2 ; }
            StartCoroutine(TileShooterBot(tile.transform.position, tile.life));
        }

    }


    public IEnumerator TileShooterBot(Vector3 position, int life)
    {
        yield return new WaitForSeconds(Random.RandomRange(1.5f, 2f));

        while (life > 0)
        {
            if (botEnable)
            {
                Paddle.instance.Shooting(position);
                life--;
            }
            yield return new WaitForSeconds(Random.RandomRange(0.2f, 0.4f));

        }

        yield return null;
    }

    public void StartBot()
    {
        botEnable = true;
        TileSpawner.instance.onTileSpawned += ShootTile;
    }








}
