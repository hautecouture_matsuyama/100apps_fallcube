﻿using UnityEngine;
using System.Collections;

public class Bomb : MonoBehaviour
{

    float speed; //Speed of the tile
    public float speedOffset;
    public int life = 10;
    public TextMesh lifeText;
    // Use this for initialization
    public Rigidbody2D rigid;
    public SpriteRenderer spriteRenderer;

    public GameObject hitParticle;
    public GOBounceEffect bounceEffect;
    void Awake()
    {

        UpdateLifeText();

        speed = speedOffset;
        rigid.velocity = Vector3.down * speed; //Move tiles

    }

    void Start()
    {

    }


    void UpdateLifeText()
    {
        lifeText.text = life.ToString();
    }

    void OnTriggerEnter2D(Collider2D coll)
    {
        if (coll.transform.tag == "Ball" || coll.transform.tag == "NoDamageBall")
        {
            life--;
            UpdateLifeText();
            bounceEffect.ExecuteEffect();

            if (life <= 0)
            {
                Spawnable[] spawnedObjects = GameObject.FindObjectsOfType<Spawnable>();

                foreach (Spawnable spawnedObject in spawnedObjects)
                {
                    Instantiate(hitParticle, spawnedObject.transform.position, spawnedObject.transform.rotation); //Play hit particle
                    Destroy(spawnedObject.gameObject);

                    if (spawnedObject.tag == "Tile")
                    {
                        Tile tile = spawnedObject.GetComponent<Tile>();
                        ScoreHandler.instance.increaseScore(tile.life);
                    }

                }

                Ball[] balls = GameObject.FindObjectsOfType<Ball>();
                foreach(Ball ball in balls)
                {
                    if (ball.transform.parent.name.Contains("BulletsContainer"))
                    {
                        Destroy(ball.gameObject);
                    }
                }

                Paddle.instance.setCannotShootForSeconds();
                TileSpawner.instance.PauseSpawnForSeconds();

                InGameCanvas.instance.showBoomBabyText();
                Destroy(this.gameObject);
            }
           
        }
    }


    


}
