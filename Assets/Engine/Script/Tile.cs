﻿using UnityEngine;
using System.Collections;

public class Tile : MonoBehaviour {

    float speed; //Speed of the tile
    public float speedOffset;
    public int life = 1;
    public TextMesh lifeText;
    // Use this for initialization
    public Rigidbody2D rigid;
    public GameObject graphicCube;
    public SpriteRenderer tileSpriteRenderer;   
    public Color StartColor;
    public Color NoLifeColor;
    public Color CurrentColor;
    float damageOffsetColor;
    float lerperColor;
    public GOBounceEffect bounceEffect;
    public Material transparentMaterial;
    public MeshRenderer cubeRenderer;
   public BoxCollider2D collide;
public    ParticleSystem[] particles;
    public GameObject dangerParticle;


    void Awake () {

        life = Random.RandomRange(1, TileLifeHandler.instance.CurrentMaxHealth + 1);

        if(Bot.instance.botEnable)
        {
            life = Mathf.Clamp(life, 1, 2);
        }


        UpdateLifeText();
        speed = speedOffset / (1+life/3);
        rigid.velocity = Vector3.down * speed; //Move tiles

        CurrentColor = StartColor;
        tileSpriteRenderer.color = CurrentColor;

        damageOffsetColor =(float) 1 / life;
        lerperColor = 1;
        if (life == TileLifeHandler.instance.maxHealth) dangerParticle.SetActive(true);

    }

    void Start()
    {

    }



    void UpdateColorOnDamage() {
        lerperColor -= damageOffsetColor;
        lerperColor = Mathf.Clamp(lerperColor, 0, 1);

        CurrentColor = Color.Lerp(NoLifeColor, StartColor, lerperColor);
        tileSpriteRenderer.color = CurrentColor;

    }

    void UpdateLifeText() {

        lifeText.text = life.ToString();
    }

	void OnTriggerEnter2D(Collider2D coll){	
        if (coll.transform.tag == "Ball" || coll.transform.tag == "NoDamageBall") {
            bounceEffect.ExecuteEffect();
            life--;
            UpdateLifeText();
            UpdateColorOnDamage();
            if (life <= 0) DieRoutine();
        }
	}


    bool dieRutineActivated;

    void DieRoutine() {
        if (dieRutineActivated) return;
        StartCoroutine(deathRoutine());

        dieRutineActivated = true;
    }

    IEnumerator deathRoutine() {
       
        graphicCube.transform.parent = null;
        foreach (ParticleSystem part in particles)
        {
            part.startLifetime=0f;
        }
        collide.enabled = false;
        cubeRenderer.material = transparentMaterial;
        StartCoroutine(faderRoutine());
      Rigidbody rigid = graphicCube.AddComponent<Rigidbody>();
        graphicCube.AddComponent<SelfDestroy>();
        Vector3 randomCircle = Random.onUnitSphere * 40;
        randomCircle.y = Mathf.Abs(randomCircle.y);
        rigid.AddForce(randomCircle, ForceMode.Impulse);
        rigid.AddTorque(randomCircle, ForceMode.Impulse);
        yield return new WaitForSeconds(3);
        Destroy(this.gameObject);

    }


    IEnumerator faderRoutine()
    {
        float lerper = 0;
        float lerperTime = 0.35f;
        Material shared = cubeRenderer.material;

        Color startcolor = shared.color;
        Color fadeColor = startcolor;
        fadeColor.a = 0.5f;

        while (lerper <= 1) {
            lerper += Time.deltaTime / lerperTime;
            shared.color = Color.Lerp(startcolor, fadeColor, lerper);
            yield return new WaitForEndOfFrame();
        }

    }


}
