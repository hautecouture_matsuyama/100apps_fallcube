﻿using UnityEngine;
using System.Collections;


public class Game_ButtonFunctions : MonoBehaviour {

	public GameController gameController;
	public void Pause(){
		PauseController.instance.Pause (); //Pause Game
	}
	
	public void Home(){
		Application.LoadLevel ("StartMenu"); //Load StartMenu Scene
	}
	
	public void Restart(){
	//	Application.LoadLevel (Application.loadedLevel); //Reload Scene
		gameController.RestartEntireGame ();
	}

	public void Rate(){
		//Put your URL
		Application.OpenURL ("https://play.google.com/store/apps/details?id=com.example"); 
	}

}
