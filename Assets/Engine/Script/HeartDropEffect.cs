﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class HeartDropEffect : MonoBehaviour {

    public Image image;
    public float duration;

    public Color originalColor;

	// Use this for initialization
	void Start () {
        originalColor = image.color;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void startEffect()
    {
        StartCoroutine(redDropEffect());
    }

    public IEnumerator redDropEffect()
    {
        image.color = Color.red;
        yield return new WaitForSeconds(1);
        image.color = originalColor;
    }

}
