﻿using UnityEngine;
using System.Collections;

public class TileSpeedHandler : MonoBehaviour {

    public static TileSpeedHandler instance;

    public float speedOffset = 0;
    float speedIncrement = 0.1f;

    public float secondsToIncreaseSpeed = 5f;

    public float maxSpeedOffset;

    public float secondsToReachMaxSpeedOffset = 20;

    public void Awake()
    {
        instance = this;
    }

    // Use this for initialization
    void Start () {
        speedIncrement = ((maxSpeedOffset - speedOffset) / secondsToReachMaxSpeedOffset) * secondsToIncreaseSpeed;
        StartCoroutine(IncreaseSpeedOverTime());
	}
	
	// Update is called once per frame
	void Update () {
	
	}


    public IEnumerator IncreaseSpeedOverTime()
    {
        while (true)
        {
            yield return new WaitForSeconds(secondsToIncreaseSpeed);

            speedOffset += speedIncrement;

            if(speedOffset > maxSpeedOffset && GameController.instance.gameMode != GameController.GameMode.TIMEATTACK)
            {
                speedOffset = maxSpeedOffset;
            }
        }
    }


}
