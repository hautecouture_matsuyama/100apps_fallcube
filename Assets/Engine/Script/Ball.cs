﻿using UnityEngine;
using System.Collections;

public class Ball : MonoBehaviour
{

    public float speed; //Speed of the ball
    public GameObject hitParticle; //Particle when hits the tiles
    public GameObject endParticle;
    public ParticleSystem taleParticle;
    public SpriteRenderer spriteRenderer;
    public Rigidbody2D rigidBody;
    public bool cannotBeDestroyed;


    // Use this for initialization
    void Start()
    {

        if (transform.parent == null)
        {
            rigidBody.velocity = Vector3.up * speed; //Move the ball
            transform.SetParent(BulletsContainer.instance.transform); // Set the ball parent
        }

    }

    void Update()
    {
        if (transform.parent.name.Contains("BulletsContainer") && !taleParticle.gameObject.activeInHierarchy)
        {
            taleParticle.gameObject.SetActive(true);
        }

        if (!transform.parent.name.Contains("BulletsContainer") && taleParticle.gameObject.activeInHierarchy)
        {
            taleParticle.gameObject.SetActive(false);
        }

    }

    void OnTriggerEnter2D(Collider2D coll)
    {

        string collTag = coll.transform.tag;

        if (collTag == "Tile" || collTag == "Bomb")
        { //When hits tiles
            if (!Player.instance.isDead)
            { //If player alive
                if (!Bot.instance.botEnable)
                {
                    ScoreHandler.instance.increaseScore(1); //add score
                }
            }
            Instantiate(hitParticle, transform.position, transform.rotation); //Play hit particle
                                                                              //SoundManager.instance.PlaySingle(hitSound); //Play hit sound
                                                                              //	Destroy(coll.transform.gameObject); //Destroy tile
            Destruction(); //Destroy this ball
        }

        if (collTag == "Coin" || collTag == "Heart" || collTag == "Clesidra")
        {
            transform.tag = "NoDamageBall";
            spriteRenderer.color = Color.white;
            taleParticle.startColor = Color.white;
            rigidBody.velocity = Vector3.up * speed / 3;
        }
    }


    void Destruction()
    {
        Destroy(gameObject);
        ParticleSystem part = GetComponentInChildren<ParticleSystem>();
        part.gameObject.transform.parent = null;
        part.gameObject.AddComponent<SelfDestroy>();
    }

    public GameObject EndParticle()
    {
        return endParticle;
    }

}
