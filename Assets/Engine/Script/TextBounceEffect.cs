﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TextBounceEffect : MonoBehaviour {

    public Text text;
    public TextMesh textMesh;

    string currentValue;
    string lastValue;


    Vector3 originalScale;
    public float increasementPercentage = 0.2f;
    public float lerpSeconds = 0.05f;

 
	// Use this for initialization
	void Start () {
        originalScale = transform.localScale;

        GetValue();

        lastValue = currentValue;



	}

    void GetValue() {

        if (text != null)
        {
            currentValue = text.text;
        }
        else {
            currentValue = textMesh.text;

        }


    }


    // Update is called once per frame
    void Update () {


        GetValue();


        if (lastValue != currentValue) {

            if (lastValue != "1")
            {
                StartCoroutine(effect());
            }
           
            // do stuff
            lastValue = currentValue;

        }
    }



    public IEnumerator effect() {


        float passedTime = 0;

        transform.localScale = originalScale;
        Vector3 localscale = originalScale;
        Vector3 targetscale = originalScale;

        targetscale += new Vector3(increasementPercentage, increasementPercentage, increasementPercentage);

        while (passedTime < lerpSeconds) {
            passedTime += Time.deltaTime/Time.timeScale;
            localscale = Vector3.MoveTowards(localscale, targetscale, (Time.deltaTime/Time.timeScale) / lerpSeconds);
            transform.localScale = localscale;
            yield return new WaitForEndOfFrame();

        }

        passedTime = 0;

        while (passedTime < lerpSeconds)
        {
            passedTime += Time.deltaTime / Time.timeScale;
            localscale = Vector3.MoveTowards(localscale, originalScale, (Time.deltaTime / Time.timeScale) / lerpSeconds);
            transform.localScale = localscale;
            yield return new WaitForEndOfFrame();

        }


        yield return null;



    }





}
