﻿using UnityEngine;
using System.Collections;

public class TileSpawner : MonoBehaviour
{

    public GameObject tile;
    public float yPosition; //Y postion for spawn
    public float spawnWait; //Wait time for spawn
    private float maxWidth; //Maxwidth of x position for spawn
    public float MinSpawnWait;
    public float secondsToReachTheMaximumDifficulty;


    bool canSpawnObjects = true;

    public static TileSpawner instance;

    public GameObject lastSpawnedTile;

    public delegate void TileSpawned();
    public event TileSpawned onTileSpawned;


    public GameObject coin;
    public int coinSpawnProbability = 10;

    public GameObject heart;
    public int heartSpawnProbability = 10;

    public GameObject clesidra;
    public int clesidraSpawnProbability = 5;

    public GameObject bomb;
    public int bombSpawnProbability = 0;

    public GameObject hitParticle;

    public GameObject clockParticle;

    public GameObject endCubeParticle;

    public GameObject endBallParticle;

    // Use this for initialization


    void Awake()
    {
        instance = this;
    }

    void Update()
    {
        //DECREASING THE SPAWN WAIT TO THE MINIMIUM SPAWN WAIT
        spawnWait = Mathf.MoveTowards(spawnWait, MinSpawnWait, Time.deltaTime / secondsToReachTheMaximumDifficulty);
    }

    void Start()
    {



        maxWidth = 8.5f;

        StartCoroutine(SpawnTile(0));
    }

   public IEnumerator SpawnTile(float initialDelay)
    {
        yield return new WaitForSeconds(initialDelay);

        while (true)
        {
            if (Player.instance.isDead)
            { //If the player dies.
                break; //Stop Loop
            }

            if (canSpawnObjects)
            {


               lastSpawnedTile = SpawnObject(tile);

                if (onTileSpawned != null)
                {
                    onTileSpawned();
                }

                if (!Bot.instance.botEnable)
                {
                    if (haveToSpawnObject(coinSpawnProbability))
                    {
                        SpawnObject(coin);
                    }

                    if (haveToSpawnObject(heartSpawnProbability))
                    {
                        SpawnObject(heart);
                    }

                    if (haveToSpawnObject(clesidraSpawnProbability))
                    {
                        SpawnObject(clesidra);
                    }

                    if (haveToSpawnObject(bombSpawnProbability))
                    {
                        SpawnObject(bomb);
                        InGameCanvas.instance.showBombText();
                    }
                }

                yield return new WaitForSeconds(spawnWait); //Wait
            }
            yield return null;
        }
    }




    public GameObject SpawnObject(GameObject gameObject)
    {
        Vector3 spawnPosition = new Vector3(Random.Range(-maxWidth, maxWidth), yPosition, 0.0f);
        Quaternion spawnRotation = Quaternion.identity;
        GameObject newObject = (GameObject)Instantiate(gameObject, spawnPosition, spawnRotation); //Spawn tiles
        newObject.transform.parent = transform;


        return newObject;
    }



    public bool haveToSpawnObject(int objectSpawnProbability)
    {
        int random = Random.Range(1, 100);
        if (random <= objectSpawnProbability)
        {
            return true;
        }
        else
        {
            return false;
        }
    }


    public void PauseSpawnForSeconds()
    {
        StartCoroutine(startPauseSpawnForSeconds());
    }

    public IEnumerator startPauseSpawnForSeconds()
    {
        canSpawnObjects = false;
        yield return new WaitForSeconds(0.5f);
        canSpawnObjects = true;
    }


    public void ExplodeAllSpawnedObjects(Transform except = null)
    {
       for (int i = 0; i < transform.childCount; i++)
        {
           
            Transform child = transform.GetChild(i);
            if (child != except)
            {
                Instantiate(hitParticle, child.position, child.rotation); //Play hit particle
                Destroy(child.gameObject);
            }
        }
    }

    public void ClockGet(Transform except = null)
    {
        Instantiate(clockParticle, except.position, except.rotation); //Play hit particle
                                                                      //Destroy(except.gameObject);
    }

    public void EndCubeParticre(Transform except = null)
    {
        Instantiate(endCubeParticle, except.position, except.rotation);
    }

    public void EndBallParticle(GameObject except = null)
    {
        Ball ball = except.gameObject.GetComponent<Ball>();
        GameObject particle = ball.endParticle;
        Instantiate(particle, except.transform.position, except.transform.rotation);
    }

}
