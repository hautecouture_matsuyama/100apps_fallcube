﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class InGameCanvas : MonoBehaviour {

    public static InGameCanvas instance;

    public ShakeHeartEffect shakeFX;
    public HeartDropEffect heartDropFX;

    public Text healtText;
    public Text scoreText;
    public Text specialpointText;


    public GameObject[] warningTexts;
    public GameObject ouchText;
    public GameObject bombText;
    public GameObject timeWarpText;
    public GameObject boomBabyText;

    void Awake()
    {
        instance = this;
    }

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

        healtText.text = Player.instance.health.ToString();
        scoreText.text = ScoreHandler.instance.score.ToString();
        specialpointText.text = ScoreHandler.instance.specialPoints.ToString();
	}

    public void showOuchText()
    {
        try
        {
            if (this.isActiveAndEnabled)
            StartCoroutine(showOuchTextCoroutine());

        }
        catch { }
    }

    private IEnumerator showOuchTextCoroutine()
    {


        disableOtherWarningTexts();
        ouchText.SetActive(true);
        yield return new WaitForSeconds(1f);
        ouchText.SetActive(false);
        yield return null;
    }

    public void showBombText()
    {
        StartCoroutine(showBombTextCoroutine());
    }


    private IEnumerator showBombTextCoroutine()
    {
        disableOtherWarningTexts();
        bombText.SetActive(true);
        yield return new WaitForSeconds(1.5f);
        bombText.SetActive(false);
        yield return null;
    }

    public void showTimeWarpText()
    {
        StartCoroutine(showTimeWarpTextCoroutine());
    }


    private IEnumerator showTimeWarpTextCoroutine()
    {
        disableOtherWarningTexts();
        timeWarpText.SetActive(true);
        yield return new WaitForSeconds(2f);
        timeWarpText.SetActive(false);
        yield return null;
    }


    public void showBoomBabyText()
    {
        StartCoroutine(showBoomBabyTextCoroutine());
    }


    private IEnumerator showBoomBabyTextCoroutine()
    {
        disableOtherWarningTexts();
        boomBabyText.SetActive(true);
        yield return new WaitForSeconds(2f);
        boomBabyText.SetActive(false);
        yield return null;
    }

    private void disableOtherWarningTexts()
    {
        foreach(GameObject text in warningTexts)
        {
            text.SetActive(false);
        }
    }

    public void OnPauseButtonClick()
    {
        GUIManager.instance.ShowPauseGUI();
    }


    public void OnHelpButtonClick()
    {
        GUIManager.instance.ShowTutorialGUI();
    }

}
