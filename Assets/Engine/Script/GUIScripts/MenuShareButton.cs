﻿using UnityEngine;
using System.Collections;

public class MenuShareButton : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void OnButtonClick()
    {



		Application.OpenURL ("fb://page/593740567494548");

		StartCoroutine(UnlockHiddenBall());

    }

	public IEnumerator UnlockHiddenBall()
    {
		yield return new WaitForSeconds (1);

        ShopItem hiddenBall = ShopHandler.instance.shopItems[ShopHandler.instance.shopItems.Count - 1];
        hiddenBall.Buy();
        ShopHandler.instance.SetShopItemToUse(hiddenBall);


    }

}
