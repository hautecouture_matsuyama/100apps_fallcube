﻿using UnityEngine;
using System.Collections;

public class MainMenuGUI : MonoBehaviour {

    public Vector2 positionWhenHidden;
    public float hideVelocity;
    public RectTransform elasticGUIObjects;

    
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void onTapToStart()
    {
        StartCoroutine(HideAndStart());     
    }


    public IEnumerator HideAndStart()
    {
    /*    elasticGUIObjects.GetComponent<GUIElement>().ForceStop();

        while(elasticGUIObjects.anchoredPosition != positionWhenHidden)
        {
            elasticGUIObjects.anchoredPosition = Vector2.MoveTowards(elasticGUIObjects.anchoredPosition, positionWhenHidden, hideVelocity*Time.deltaTime);
            yield return new WaitForEndOfFrame();
        }
        */
        gameObject.SetActive(false);
        GameController.instance.StartGame();
        yield return null;
    }


    public void OnShopButtonClick()
    {
        SoundManager.instance.PlayMenuButtonSound();
        GUIManager.instance.ShowShopGUI();
    }

    public void OnLeaderboardButtonClick()
    {
       // Leaderboard.instance.showLeaderboard();
    }

    public void OnRateButtonClick()
    {
        RateManager.instance.rateGame();
    }

    public void OnDisable()
    {
        elasticGUIObjects.anchoredPosition = positionWhenHidden;
    }

}
