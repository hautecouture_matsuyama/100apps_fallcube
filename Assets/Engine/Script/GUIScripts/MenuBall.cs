﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MenuBall : MonoBehaviour {

    Image ballImage;

    void Awake()
    {
        ballImage = GetComponentInChildren<Image>();
    }

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        if (ShopHandler.instance != null)
        {
            ballImage.sprite = ShopHandler.instance.shopItemToUse.sprite.sprite;
        }
    }

    void OnEnable()
    {
       
     
    }




}
