﻿using UnityEngine;
using System.Collections;

public class MenuCharacterAnim : MonoBehaviour {


    public float waitTimeBeforeStart;
    public float waitTimeBetweenEachMove = 0.2f;
    public float minX;
    public float maxX;

    RectTransform rectTransform;


    void Awake()
    {
        rectTransform = GetComponent<RectTransform>();
    }

	// Use this for initialization
	void Start () {
        
	}
	
	// Update is called once per frame
	void Update () {
	
	}


    public IEnumerator DOAnim()
    {
        yield return new WaitForSeconds(waitTimeBeforeStart);
        while (true)
        {
            rectTransform.anchoredPosition = GetNewRandomPosition();
            yield return new WaitForSeconds(waitTimeBetweenEachMove);
        }
    }


    Vector3 GetNewRandomPosition()
    {
        Vector3 position = rectTransform.anchoredPosition;
        Vector3 newPosition = position;
        float random = Random.Range(minX, maxX);

        newPosition.x = random;

        return newPosition;
    }

    void OnEnable()
    {
        StartCoroutine(DOAnim());
    }

}
