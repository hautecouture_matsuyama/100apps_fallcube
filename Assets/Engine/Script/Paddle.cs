﻿using UnityEngine;
using System.Collections;

public class Paddle : MonoBehaviour
{


    public static Paddle instance;
    public MainBallContainer MainBallContainer; //For shoot the ball
    public AudioSource shotSound;



    bool canShoot = true;

    void Awake()
    {
        instance = this;
    }



    public void Shooting(Vector3 pos)
    {
        if (canShoot)
        {
       

            SoundManager.instance.PlayAudioSource(shotSound);
            Vector3 position = pos;
            position.y = this.transform.position.y;
            position.z = this.transform.position.z;

         //   Debug.Log(position);

            Ball newBall = (Ball)Instantiate(MainBallContainer.GetBallChild(), position, Quaternion.identity); //Spawn the ball          
            transform.position = position;
        }
    }

    public void setCannotShootForSeconds()
    {
        StartCoroutine(setCannotShoot());
    }


    public IEnumerator setCannotShoot()
    {
        canShoot = false;
        yield return new WaitForSeconds(0.5f);
        canShoot = true;
    }


}
