﻿using UnityEngine;
using System.Collections;

public class ColorSwitchBackground : MonoBehaviour {

    public ColorPattern[] patterns;
    ColorPattern currentPattern;
    ColorPattern savedPattern;


	public Color currentColor1;
    public Color currentColor2;
  
    public Color targetColor1;
    public Color targetColor2;

    public Camera cam;
    public SpriteRenderer sprite;
	public float fadeSpeed = 0.2f;
    // Use this for initialization
    public int forcePatternNumber = -1;

    void SwitchRandomPattern()

    {

        if (forcePatternNumber != -1)
        {
            currentPattern = patterns[forcePatternNumber];
            return;
        }


        currentPattern = patterns[Random.Range(0, patterns.Length)];
    while (currentPattern == savedPattern)
        {
            currentPattern = patterns[Random.Range(0, patterns.Length)];

        }
        savedPattern = currentPattern;
     

    }

	void Start () {

        SwitchTargetColor();       

        StartCoroutine(changeColorConstantly());

        currentColor1 = targetColor1;
        currentColor2 = targetColor2;

	}

	void SwitchTargetColor(){

        SwitchRandomPattern();

        targetColor1 = currentPattern.colorTop;
        targetColor2 = currentPattern.colorBottom;

    }
	
	// Update is called once per frame
	void Update () {


            currentColor1 = Color.Lerp (currentColor1, targetColor1, Time.deltaTime * fadeSpeed);
            currentColor2 = Color.Lerp(currentColor2, targetColor2, Time.deltaTime * fadeSpeed);

        cam.backgroundColor = currentColor1;
        sprite.color = currentColor2;
        RenderSettings.ambientLight = currentColor2;





    }


    IEnumerator changeColorConstantly()
    {
        while (true) {

            yield return new WaitForSeconds(15);
            SwitchTargetColor();

        }


    }
}
