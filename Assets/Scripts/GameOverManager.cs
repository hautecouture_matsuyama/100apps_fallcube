﻿using UnityEngine;
using System.Collections;

public class GameOverManager : MonoBehaviour {
    public static GameOverManager instance;
	// Use this for initialization

        void Awake()
    {
        instance = this;

    }

    public void GameOverEvent(GameObject gmobject) {

        if (GameController.instance.gameState!=GameController.GameState.gameover)
        StartCoroutine(GameOverEventRoutine(gmobject));
    }
    IEnumerator GameOverEventRoutine(GameObject obj)
    {
        Player.instance.isDead = true;
        GameController.instance.gameState = GameController.GameState.gameover;
        TileSpawner.instance.ExplodeAllSpawnedObjects(obj.transform);
        TileSpawner.instance.StopAllCoroutines();



        float waitSegmentValue = 0.25f;

        obj.GetComponent<Rigidbody2D>().isKinematic = true;
        yield return new WaitForSeconds(waitSegmentValue*2f);
        obj.SetActive(false);
        yield return new WaitForSeconds(waitSegmentValue);
        obj.SetActive(true);
        yield return new WaitForSeconds(waitSegmentValue);
        obj.SetActive(false);
        yield return new WaitForSeconds(waitSegmentValue);
        obj.SetActive(true);
        yield return new WaitForSeconds(waitSegmentValue);
        obj.SetActive(false);
        Destroy(obj);
        yield return new WaitForSeconds(waitSegmentValue);

   

        GameController.instance.GameOver();




    }


}
