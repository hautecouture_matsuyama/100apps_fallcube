﻿using UnityEngine;
using System.Collections;

public class constantRotate : MonoBehaviour {
   public bool yAngle=true;
    public float rotPower = 180;
    public bool zAngle = false;
    public bool xAngle = false;

    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        if (yAngle)
        transform.Rotate(0, rotPower * Time.deltaTime, 0);
        if (zAngle)
            transform.Rotate(0, 0, rotPower * Time.deltaTime);
        if (xAngle)
            transform.Rotate(rotPower * Time.deltaTime, 0, 0);



    }
}
