﻿using UnityEngine;
using System.Collections;

[System.Serializable]

public class ColorPattern {

    public Color colorTop;
    public Color colorBottom;

    public ColorPattern(Color top,Color bottom)
    {
        colorTop = top;
        colorBottom = bottom;
    }
}
