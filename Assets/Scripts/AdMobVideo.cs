﻿using UnityEngine;
using System.Collections;
using GoogleMobileAds.Api;
using System;

public class AdMobVideo : MonoBehaviour {

    public string Android_Banner;
    public string ios_Banner;

    public string testid;

    public bool testmode;

    RewardBasedVideoAd rewardBasedVideo;

    public int minCoinsToRewardOnVideoWatched = 75;
    public int maxCoinsToRewardOnVideoWatched = 100;

    private static AdMobVideo mInstace;

    public static AdMobVideo Instance
    {
        get
        {
            if (mInstace == null)
                mInstace = new AdMobVideo();
            return mInstace;
        }

    }

    bool rewardBasedEventHandlersSet = false;

    AdRequest request;

    void Awake()
    {
        if (mInstace == null)
        {

            mInstace = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {

            Destroy(gameObject);
        }
    }

    // Use this for initialization
    void Start () {
        RequestRewardBasedVideo();
    }
	
	// Update is called once per frame
	void Update () {
	
	}

    private void RequestRewardBasedVideo()
    {
#if UNITY_ANDROID
    string adUnitId = Android_Banner;
#elif UNITY_IPHONE
    string adUnitId = ios_Banner;
#else
        string adUnitId = "unexpected_platform";
#endif

         rewardBasedVideo = RewardBasedVideoAd.Instance;

        if (testmode)
        {
            request = new AdRequest.Builder().AddTestDevice(testid).Build();
        }
        else
        {
            request = new AdRequest.Builder().Build();
        }
        rewardBasedVideo.LoadAd(request, adUnitId);

            // has rewarded the user.
            rewardBasedVideo.OnAdRewarded += HandleRewardBasedVideoRewarded;

            rewardBasedVideo.OnAdClosed += HandleRewardBasedVideoRewardedClosed;

            rewardBasedVideo.OnAdFailedToLoad += HandleRewardBasedVideoRewardedFailed;

            //rewardBasedEventHandlersSet = true;
        
    }

    public delegate void Delegate();

    public Delegate delegete;

    public void ShowVideo(Delegate delegateMethod)
    {
        if (rewardBasedVideo.IsLoaded())
        {
            delegete = delegateMethod;
            rewardBasedVideo.Show();
        }
    }

    // 報酬受け渡し処理
    public void HandleRewardBasedVideoRewarded(object sender, Reward args)
    {
        //player.IncentiveSpeed();
        //rewardPanel.SetActive(true);
        //tm.ViewVideo();
        //ViewVideo();
        delegete();
        Debug.Log("リワードゲット!!");

    }

    // 動画広告リロード
    public void HandleRewardBasedVideoRewardedClosed(object sender, System.EventArgs args)
    {
        RequestRewardBasedVideo();
    }

    // ロード失敗時
    public void HandleRewardBasedVideoRewardedFailed(object sender, AdFailedToLoadEventArgs args)
    {
        StartCoroutine(_waitConnectReward());
    }

    // ロードに失敗した場合、30秒待ってから再ロードをする
    IEnumerator _waitConnectReward()
    {
        while (true)
        {
            yield return new WaitForSeconds(30.0f);

            // 通信ができない場合は、リロードしない
            if (Application.internetReachability != NetworkReachability.NotReachable)
            {
                RequestRewardBasedVideo();
                break;
            }


        }
    }

    void ViewVideo()
    {
    }

    public int GetCoinsToRewardOnVideoWatched()
    {
        return UnityEngine.Random.Range(minCoinsToRewardOnVideoWatched, maxCoinsToRewardOnVideoWatched);
    }
}
