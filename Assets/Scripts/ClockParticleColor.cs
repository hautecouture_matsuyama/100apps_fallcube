﻿using UnityEngine;
using System.Collections;

public class ClockParticleColor : MonoBehaviour {

    private ParticleSystem particle;

    public Color color;

	// Use this for initialization
	void Start () {
        particle = this.gameObject.GetComponent<ParticleSystem>();
        particle.startColor = color;
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
