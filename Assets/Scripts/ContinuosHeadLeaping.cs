﻿using UnityEngine;
using System.Collections;

public class ContinuosHeadLeaping : MonoBehaviour {
    float leapTime = 1;
    float lerper = 0;
    public Vector3 leapright, leapleft;
	// Use this for initialization
	void OnEnable () {
        StartCoroutine(leapingCoroutine());


        
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    IEnumerator leapingCoroutine() {


        while (true)
        {
            lerper = 0;

            while (lerper < 1) {
                lerper += Time.deltaTime / leapTime;
                transform.eulerAngles = Vector3.Lerp(leapleft, leapright, lerper);
                yield return new WaitForEndOfFrame();
            }

            while (lerper > 0)
            {
                lerper -= Time.deltaTime / leapTime;
                transform.eulerAngles = Vector3.Lerp(leapleft, leapright, lerper);
                yield return new WaitForEndOfFrame();

            }

            yield return null;




        }



    }





}
