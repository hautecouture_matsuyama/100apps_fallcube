﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ShopGUI : MonoBehaviour {

    public static ShopGUI instance;

    public Text selectedItemText;
    public Text selectedItemPriceText;
    public Text specialPointsText;

    public GameObject unlockButton;
    public GameObject playButton;
	public GameObject facebookButton;
	public GameObject twitterButton;

    public Image bottomSpecialPointImage;

    void Awake()
    {
        instance = this;
    }

	// Use this for initialization
	void Start () {
        unlockButton.SetActive(false);
        playButton.SetActive(false);
	}
	
	// Update is called once per frame
	void Update () {

        selectedItemText.text = ShopHandler.instance.selectedItem.name;
        selectedItemPriceText.text = "" + ShopHandler.instance.selectedItem.price;
        specialPointsText.text = ScoreHandler.instance.specialPoints.ToString();


		if (ShopHandler.instance.selectedItem.unlocked) {

			if (bottomSpecialPointImage.enabled) {
				bottomSpecialPointImage.enabled = false;
				selectedItemPriceText.enabled = false;
			}
           

			DeactivateAllButtons ();

			if (!playButton.activeSelf) {
				playButton.SetActive (true);
			}

			if (!selectedItemPriceText.gameObject.activeInHierarchy) {
				selectedItemPriceText.gameObject.SetActive (true);
			}

			if (!bottomSpecialPointImage.gameObject.activeInHierarchy) {
				bottomSpecialPointImage.gameObject.SetActive (true);
			}


		} else {
			
			if (ShopHandler.instance.selectedItem.unlockableWithSpecialPoints) {

				if (!bottomSpecialPointImage.enabled) {
					bottomSpecialPointImage.enabled = true;
					selectedItemPriceText.enabled = true;
				}

				DeactivateAllButtons ();

				if (!unlockButton.activeSelf) {
					unlockButton.SetActive (true);
				}

			}else{

				if (bottomSpecialPointImage.enabled) {
					bottomSpecialPointImage.enabled = false;
					selectedItemPriceText.enabled = false;
				}

	
				DeactivateAllButtons ();	

				if (ShopHandler.instance.selectedItem.unlockableWithFacebook) {
					facebookButton.SetActive (true);
				}

				if (ShopHandler.instance.selectedItem.unlockableWithTwitter) {
					twitterButton.SetActive (true);
				}

			}

		}
    }

	public void DeactivateAllButtons(){
		if (playButton.activeSelf) {
			playButton.SetActive (false);
		}
		if (unlockButton.activeSelf) {
			unlockButton.SetActive (false);
		}
		if (facebookButton.activeSelf) {
			facebookButton.SetActive (false);
		}
		if (twitterButton.activeSelf) {
			twitterButton.SetActive (false);
		}
	}

    public void OnBackButtonClick()
    {
        SoundManager.instance.PlayMenuButtonSound();

        GUIManager.instance.ShowMainMenuGUI();
        ShopHandler.instance.Deactivate();
    }

    public void onUnlockButtonClick()
    {
        SoundManager.instance.PlayMenuButtonSound();

        if (ShopHandler.instance.selectedItem.price <= ScoreHandler.instance.specialPoints)
        {
            ShopHandler.instance.selectedItem.Buy();
        }
    }

    public void onPlayButtonClick()
    {
        SoundManager.instance.PlayMenuButtonSound();

        GUIManager.instance.ShowMainMenuGUI();
        ShopHandler.instance.Deactivate();
        ShopHandler.instance.SetShopItemToUse(ShopHandler.instance.selectedItem);
    }

	public void OnFacebookButtonClick(){
		StartCoroutine(VisitUrlToUnlock(ShopHandler.instance.facebookURL,"Thanks", "Thanks for liking us! New ball unlocked!"));
	}



	public void OnTwitterButtonClick(){
		StartCoroutine(VisitUrlToUnlock(ShopHandler.instance.twitterURL,"Thanks", "Thanks for following us! New ball unlocked!"));					
	}

	IEnumerator VisitUrlToUnlock(string url, string messageTitle, string messageBody){
		Application.OpenURL (url);
		yield return new WaitForSeconds (1);
		MobileNativeMessage msg = new MobileNativeMessage (messageTitle,messageBody);
		ShopHandler.instance.selectedItem.Buy ();
	}

}
