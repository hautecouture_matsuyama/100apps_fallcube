﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.Advertisements;

public class GameOverGUI : MonoBehaviour {

    public Text scoreText;
    public Text highScoreText;

    public Text pointText;


    public Text coinText;

    public Button GetCoinButton;

    public GameObject watingPanel;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        scoreText.text = "" + ScoreHandler.instance.score;
        highScoreText.text = "" + ScoreHandler.instance.highScore;
        coinText.text = "" + ScoreHandler.instance.specialPoints;
	}



    public void OnGetCoinButtonClick()
    {
        //if (Advertisement.IsReady ()) {
        //	UnityRewardAds.instance.ShowRewardedAd (HandleShowResult);
        //	//GetCoinButton.gameObject.SetActive (false);
        //} else {
        //	MobileNativeMessage msg = new MobileNativeMessage("Error!", "Internet connection must be enabled to use this feature");
        //}
        GetCoinButton.gameObject.SetActive(false);
        AdMobVideo.Instance.ShowVideo(HandleShowResult);
    }

    public void OnBallShopClick()
    {
        Deactivate();
        GUIManager.instance.ShowShopGUI();
    }

    public void OnRemoveAdsButtonClick()
    {
		AdRemover.instance.BuyNonConsumable ();
    }

    public void OnRestorePurchaseButtonClick()
    {
		AdRemover.instance.RestorePurchases ();
    }

    public void OnLeaderboardButtonClick()
    {
       // Leaderboard.instance.showLeaderboard();
    }

    public void OnShareButtonClick()
    {
        ShareManager.instance.share();
    }

    public void OnPlayButtonClick()
    {
        Deactivate();
        SoundManager.instance.PlayMenuButtonSound();

        GUIManager.instance.ShowMainMenuGUI();
    }

    public void Deactivate()
    {
        gameObject.SetActive(false);
    }


    private void HandleShowResult()
    {
        //switch (result)
        //{
        //    case ShowResult.Finished:
        //        ScoreHandler.instance.increaseSpecialPoints(UnityRewardAds.instance.GetCoinsToRewardOnVideoWatched());
        //        //
        //        // YOUR CODE TO REWARD THE GAMER
        //        // Give coins etc.
        //        break;
        //    case ShowResult.Skipped:
        //        Debug.Log("The ad was skipped before reaching the end.");
        //        break;
        //    case ShowResult.Failed:
        //        Debug.LogError("The ad failed to be shown.");
        //        break;
        //}
        //ScoreHandler.instance.increaseSpecialPoints(AdMobVideo.Instance.GetCoinsToRewardOnVideoWatched());
        ScoreHandler.instance.WatingVieo();
        this.gameObject.SetActive(false);
        watingPanel.SetActive(true);
    }

    void OnEnable()
    {
        highScoreText.text = "BEST " + ScoreHandler.instance.highScore;
        pointText.text = ScoreHandler.instance.specialPoints.ToString();
        ScoreHandler.instance.EndRewardGame();
        GetCoinButton.gameObject.SetActive(true);
    }


}
