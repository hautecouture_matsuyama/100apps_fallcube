﻿using UnityEngine;
using System.Collections;
using UnityEngine.Advertisements;

public class OneMoreChanceGUI : MonoBehaviour {



    void Awake()
    {
        
    }

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void Activate()
    {
        gameObject.SetActive(true);
    }

    void Deactivate()
    {
        gameObject.SetActive(false);
    }

    public void OnOneMoreChanceButtonClick()
    {
        if (Advertisement.IsReady(UnityRewardAds.instance.rewardVideoID))
        {
            UnityRewardAds.instance.ShowRewardedAd(HandleShowResult);
        }
        else
        {
            GameController.instance.oneMoreChanceUsed = true;
        
            TileSpawner.instance.StartCoroutine(TileSpawner.instance.SpawnTile(1));
            StartCoroutine(resetGameStateToGame());
        }

        Deactivate();
    }

    IEnumerator resetGameStateToGame()
    {
        yield return new WaitForSeconds(0.25f);
        Player.instance.health += 1;
        Player.instance.isDead = false;
        GameController.instance.gameState = GameController.GameState.game;



    }

    public void OnGameOverButtonClick()
    {
        Deactivate();
        GameController.instance.oneMoreChanceUsed = true;
        GameController.instance.GameOver();
    }


    private void HandleShowResult(ShowResult result)
    {
        switch (result)
        {
            case ShowResult.Finished:
           
                GameController.instance.gameState = GameController.GameState.game;
                GameController.instance.oneMoreChanceUsed = true;
                Player.instance.health += 1;
                Player.instance.isDead = false;
                TileSpawner.instance.ExplodeAllSpawnedObjects();
                TileSpawner.instance.StartCoroutine(TileSpawner.instance.SpawnTile(1));
                // YOUR CODE TO REWARD THE GAMER
                // Give coins etc.
                break;
            case ShowResult.Skipped:

                GameController.instance.gameState = GameController.GameState.game;
                GameController.instance.oneMoreChanceUsed = true;
                Player.instance.health += 1;
                Player.instance.isDead = false;
                TileSpawner.instance.ExplodeAllSpawnedObjects();
                TileSpawner.instance.StartCoroutine(TileSpawner.instance.SpawnTile(1));
                break;
            case ShowResult.Failed:

                GameController.instance.gameState = GameController.GameState.game;
                GameController.instance.oneMoreChanceUsed = true;
                Player.instance.health += 1;
                Player.instance.isDead = false;
                TileSpawner.instance.ExplodeAllSpawnedObjects();
                TileSpawner.instance.StartCoroutine(TileSpawner.instance.SpawnTile(1));
                break;
        }
    }

}
