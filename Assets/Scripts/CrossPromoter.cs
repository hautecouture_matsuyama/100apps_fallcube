﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CrossPromoter : MonoBehaviour
{

	public static bool shown;
	public string siteToGetDataURL = "http://pastebin.com/raw/eqr7BMsq";


	public static CrossPromoter instance;

	public Image portraitImage;
	public Image landscapeImage;

	string rawDataFromServer;
	string appToPromoteData;
	string imageURL;
	string appStoreURL;

	Texture2D imageTexture;

	public bool error;
	public bool initialized;
	public bool showing;

	void Awake(){
		DontDestroyOnLoad (this.gameObject);
		instance = this;


	}

	// Use this for initialization
	void Start ()
	{
		if (!shown) {
			StartCoroutine (Initialize ());
		}
	}

	// Update is called once per frame
	void Update ()
	{


	}

	public IEnumerator FetchAppToPromoteData ()
	{
		string[] rawDatas = rawDataFromServer.Split ('\n');

		appToPromoteData = rawDatas [RandomNum (0, rawDatas.Length)].Replace ("\n", "").Replace ("\r", "");

		appStoreURL = appToPromoteData.Split ('|') [0];

		imageURL = appToPromoteData.Split ('|') [1];


		WWW download = new WWW (imageURL);
		yield return download;

		imageTexture = download.texture;   

		if (!string.IsNullOrEmpty (download.error))
			error = true;

	}

	public IEnumerator GetRawDataFromServer ()
	{
		WWW download = new WWW (siteToGetDataURL);
		yield return download;

		rawDataFromServer = download.text;
	}

	public IEnumerator Initialize ()
	{

		yield return GetRawDataFromServer ();
		yield return FetchAppToPromoteData ();
		SetGraphic ();
		initialized = true;
		shown = true;
	}



	public IEnumerator FadeIn ()
	{
		Image imgToFade = portraitImage; 

		if (Screen.orientation == ScreenOrientation.Portrait || Screen.orientation == ScreenOrientation.PortraitUpsideDown) {
			imgToFade = portraitImage; 

			imgToFade.transform.parent.gameObject.SetActive (true);
		} else {
			imgToFade = landscapeImage; 
			imgToFade.transform.parent.gameObject.SetActive (true);
		}

		Color newColor = imgToFade.color;
		; 

		while (imgToFade.color.a != 1) {
			newColor.a = Mathf.MoveTowards (imgToFade.color.a, 1f, 0.8f * Time.deltaTime);
			imgToFade.color = newColor;
			yield return new WaitForEndOfFrame ();
		}
	}


	public void SetGraphic ()
	{
		Sprite textToSprite = Sprite.Create (imageTexture, new Rect (0, 0, imageTexture.width, imageTexture.height), new Vector2 (0.5f, 0.5f));	

		if (Screen.orientation == ScreenOrientation.Portrait || Screen.orientation == ScreenOrientation.PortraitUpsideDown) {
			portraitImage.sprite = textToSprite;
		} else {
			landscapeImage.sprite = textToSprite;
		}

	}

	public int RandomNum (int min, int max)
	{
		return Random.Range (min, max);
	}


	public void OnOpenButtonClick ()
	{
		Application.OpenURL (appStoreURL);
		showing = false;
		gameObject.SetActive (false);
	}

	public void OnCloseButtonClick ()
	{
		showing = false;
		gameObject.SetActive (false);
	}


	public void Show(){
		showing = true;
		StartCoroutine (FadeIn ());
	}






}
