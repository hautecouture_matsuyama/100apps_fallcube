﻿using UnityEngine;
using System.Collections;

public class ShakeHeartEffect : MonoBehaviour {

	Vector3 originalPosition;
	public bool EnableShake;
	public float ShakeTimer = 2;
	float originalShakeTimer;
	// Use this for initialization
	void Start () {
		originalShakeTimer = ShakeTimer;
		originalPosition = transform.position;
	}
	
	// Update is called once per frame
	void Update () {
		
		if (EnableShake) {

			transform.position = originalPosition + Random.insideUnitSphere * 3f;
			if (ShakeTimer > 0) {
				ShakeTimer -= Time.deltaTime;
				if (ShakeTimer <= 0) {
					EnableShake = false;
					ShakeTimer = originalShakeTimer;
				}
			}
		
		}


	}


	public void Shake(){
		EnableShake = true;
	}

}
