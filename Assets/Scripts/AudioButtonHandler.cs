﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class AudioButtonHandler : MonoBehaviour {

    Button thisButton;
    // 1 = off 2 = on
   public Sprite spr1, spr2;


	// Use this for initialization
	void Start () {
        thisButton = GetComponent<Button>();
	}
	
	// Update is called once per frame
	void Update () {
        if (AudioListener.volume == 0) {

            thisButton.image.sprite = spr1;
                }
        else
        {
            thisButton.image.sprite = spr2;

        }
    }

    bool trigger = true        ;
    float vol;
    public void OnButClick()
    {
        trigger = !trigger;
        if (!trigger)
        {
            vol = AudioListener.volume;
            AudioListener.volume = 0;

        }
        else
        {
            AudioListener.volume = vol;


        }

    }
}
