cc.Class({
    extends: cc.Component,

    properties: {
        topBackGroundNode: cc.Node,
        underBackGroundNode: cc.Node,

        topBackGroundColorPalette: [cc.Color],
        underBackGroundPalette: [cc.Color],
        _topBackGroundTargetColor: cc.Color,
        _underBackGroundTargetColor: cc.Color,

        _palettePointer: 0,
        //色の被り防止用
        _savedPalettePointer: -1,

        _fadeTime: 0,
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        this.firstColorChange();
    },

    start() {
        this.schedule(this.colorChange, 15);
    },

    firstColorChange() {
        this.colorChange();
        this.topBackGroundNode.color = this._topBackGroundTargetColor;
        this.underBackGroundNode.color = this._underBackGroundTargetColor;
    },

    colorChange () {
        this._palettePointer = Math.floor( Math.random () * this.underBackGroundPalette.length);

        if (this._palettePointer === this._savedPalettePointer) {
            console.log("PointerShuffle")
            while (this._palettePointer === this._savedPalettePointer) {
                this._palettePointer = Math.floor( Math.random () * this.underBackGroundPalette.length);
            }
        }

        this._topBackGroundTargetColor = this.topBackGroundColorPalette[this._palettePointer];
        this._underBackGroundTargetColor = this.underBackGroundPalette[this._palettePointer];

        this._savedPalettePointer = this._palettePointer;

        this._fadeTime = 0;
    },

    update (dt) {
        this._fadeTime += dt * 0.01;
        let topColor = this.topBackGroundNode.color;
        let underColor = this.underBackGroundNode.color;
        if (!topColor.equals(this._topBackGroundTargetColor)) {
            this.topBackGroundNode.color = this.topBackGroundNode.color.lerp(this._topBackGroundTargetColor, this._fadeTime);
        }
        if (!underColor.equals(this._underBackGroundTargetColor)) {
            this.underBackGroundNode.color = this.underBackGroundNode.color.lerp(this._underBackGroundTargetColor, this._fadeTime);
        }
    },

    returnUnderBackGroundColor () {
        return this.underBackGroundNode.color;
    }
});
