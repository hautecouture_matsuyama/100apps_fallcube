const DataManager = require("DataManager");
const FbInstantGames = require("FbInstantGames");
const FbMatchGame = require("FbMatchGame");
const LeaderBoard = require("Leaderboard");
const GameOverProcessing = require("GameOverProcessing");

cc.Class({
    extends: cc.Component,

    properties: {
        dataManager: DataManager,
        fbInstantGames: FbInstantGames,
        fbMatchGame: FbMatchGame,
        leaderBoard: LeaderBoard,
        gameOverProcessing: GameOverProcessing,

        gameScreenNode: cc.Node,
        shopScreenNode: cc.Node,
        matchingScreenNode: cc.Node,
        botSensorNode: cc.Node,
        rankingNode: cc.Node,
        objectContainerNode: cc.Node,
        ballContainerNode: cc.Node,
        buttonNode: cc.Node,

        playButtonNode: cc.Node,
        matchingButtonNode: cc.Node,

        soundButtonSpriteNode: cc.Sprite,
        soundONButtonSprite: cc.SpriteFrame,
        soundOFFButtonSprite: cc.SpriteFrame,
        _isMute: false,

        _audioManagerNode: null,
        _audioManager: null,
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        this._audioManagerNode = cc.find('Canvas/AudioManager');
        this._audioManager = this._audioManagerNode.getComponent('AudioManager');

        this.fbInstantGames.createShortcut();
        this._audioManager.gameStartAudioPlay();
    },

    start () {
        console.log('joinMath:'+this.fbMatchGame.OnJoinMath());
        if (this.fbMatchGame.OnJoinMath()) {
            this.playButtonNode.setPosition(cc.v2(-150, 0));
            this.matchingButtonNode.active = true;
        }
    },

    playButtonAction () {
        this.botSensorNode.active = false;
        this.objectContainerNode.removeAllChildren();
        this.ballContainerNode.removeAllChildren();
        this.gameScreenNode.active = true;
        this.node.active = false;

        this.gameOverProcessing.switchingisMatching(false);

        this._audioManager.audioPlay(3);
    },

    matchingButtonAction () {
        this._audioManager.audioPlay(3);
        this.matchingScreenNode.active = true;
        this.node.active = false;
    },

    soundButtonAction () {
        if (this._isMute === false)
        {
            this.soundButtonSpriteNode.spriteFrame = this.soundOFFButtonSprite;
            this._isMute = true;
        }
        else
        {
            this.soundButtonSpriteNode.spriteFrame = this.soundONButtonSprite;
            this._isMute = false;
        }
    },

    titleRankingButtonAction () {
        this._audioManager.audioPlay(3);
        this.leaderBoard.sendScore(this.dataManager.returnBestScore());
        this.rankingNode.active = true;
        this.buttonNode.active = false;
    },

    shopButtonAction () {
        this._audioManager.audioPlay(3);
        this.shopScreenNode.active = true;
        this.node.active = false;
    },

    returnisMute() {
        return this._isMute;
    }
});
