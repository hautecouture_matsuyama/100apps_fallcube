cc.Class({
    extends: cc.Component,

    properties: {
        ball: cc.Node,
        itemPrice: 0,
        isUnlocked: false,
        isUnlockedByDefault: false,
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        if(this.isUnlockedByDefault)
        {
            this.isUnlocked = true;
        }
        else {
            this.isUnlocked = false;
        }
    },

    switchingisUnlockedByDefault: function () {
        this.isUnlockedByDefault = true;
    }
});
