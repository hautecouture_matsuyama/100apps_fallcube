cc.Class({
    extends: cc.Component,

    properties: {
        particlePrefab: cc.Node,

        _dataManagerNode: null,
        _dataManager: null,
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        const myParticle = this.particlePrefab.getComponent(cc.ParticleSystem);

        this._dataManagerNode = cc.find('Canvas/DataManager');
        this._dataManager = this._dataManagerNode.getComponent('DataManager');

        switch (this.node.name) {
            case "ObjectHitFallParticle":
            case "BallEndParticle":
            myParticle.startColor = this._dataManager.returnBallColor();
            break;
        }

        myParticle.resetSystem();
    },
});