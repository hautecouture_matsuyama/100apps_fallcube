const DataManager = require("DataManager");

cc.Class({
    extends: cc.Component,

    properties: {
        dataManager: DataManager,

        ball: cc.Prefab,
        block: cc.Prefab,
        diamond: cc.Prefab,
        clock: cc.Prefab,

        _randomMin: -210,
        _randomMax: 210,

        objectContainer: cc.Node,
        ballContainer: cc.Node,

        _ballColor: cc.color(255, 255, 255, 255),

        _unObjectCreate: false,

        _objectInstans: null,
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        this._ballColor = this.dataManager.returnBallColor();
    },

    randomObjectCreate (isBot) {
        if (this._unObjectCreate === true) return;
        let objectSetingRandomPositionX = Math.floor(Math.random() * (this._randomMax - this._randomMin)) + this._randomMin;
        let objectNumber = Math.floor(Math.random() * 10) + 1;
        if (isBot === true) {
            objectNumber = 0;
        }
        switch (objectNumber) {
            case 9:
            this._objectInstans = cc.instantiate(this.diamond);
            this._objectInstans.setPosition(cc.v2(objectSetingRandomPositionX, 2000));
            this.objectContainer.addChild(this._objectInstans, 0, 1);
            break;

            case 10:
            this._objectInstans = cc.instantiate(this.clock);
            this._objectInstans.setPosition(cc.v2(objectSetingRandomPositionX, 2000));
            this.objectContainer.addChild(this._objectInstans, 0, 2);
            break;

            default:
            this._objectInstans = cc.instantiate(this.block);
            this._objectInstans.setPosition(cc.v2(objectSetingRandomPositionX, 2000));
            this.objectContainer.addChild(this._objectInstans, 0, 0);
            break;
        }
    },

    ballCreate (createPosX) {
        if (this._unObjectCreate === true) return;
        
        this._objectInstans = cc.instantiate(this.ball);
        this._objectInstans.setPosition(cc.v2(createPosX, -635));
        this._objectInstans.color = this._ballColor;
        this.ballContainer.addChild(this._objectInstans, 0, 0);
    },

    unObjectCreate () {
        this._unObjectCreate = true;
    }
});
