cc.Class({
    extends: cc.Component,

    properties: {
        ballLocusParticle: cc.Node,
        ballCollider: cc.PhysicsCircleCollider,

        _dataManagerNode: null,
        _dataManager: null,

        _audioManagerNode: null,
        _audioManager: null,

        _gameScreenNode: null,
        _gameScreen: null,

        _particleCreatorNode: null,
        _particleCreator: null,

        _botSensorNode: null,

        _rigidbody: null,
        _moveSpeed: 1600,
        _isSlowMotion: false,
        _isBreakDia: false,
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        this._botSensorNode = cc.find('Canvas/ScreenCommonNode/BotSensor');

        this._dataManagerNode = cc.find('Canvas/DataManager');
        this._dataManager = this._dataManagerNode.getComponent('DataManager');
        this.node.color = this._dataManager.returnBallColor();

        this._gameScreenNode = cc.find('Canvas/GameScreenNode');
        this._gameScreen = this._gameScreenNode.getComponent('GameScreen');

        this._particleCreatorNode = cc.find('Canvas/ScreenCommonNode/ParticleCreatorNode');
        this._particleCreator = this._particleCreatorNode.getComponent('ParticleCreator');

        this._audioManagerNode = cc.find('Canvas/AudioManager');
        this._audioManager = this._audioManagerNode.getComponent('AudioManager');
        this._audioManager.audioPlay(4);
        
        this._rigidbody = this.node.getComponent(cc.RigidBody);
        this._rigidbody.linearVelocity = cc.v2(0, this._moveSpeed);
    },

    update (dt) {
        if (this._isSlowMotion === false && cc.director.getScheduler().getTimeScale() < 1) {
            this._isSlowMotion = true;
            this._moveSpeed = this._moveSpeed / 2;
            this._rigidbody.linearVelocity = cc.v2(0, this._moveSpeed);
        }
        else if (this._isSlowMotion === true && cc.director.getScheduler().getTimeScale() === 1){
            this._isSlowMotion = false;
            this._moveSpeed = this._moveSpeed * 2;
            this._rigidbody.linearVelocity = cc.v2(0, this._moveSpeed);
        }
    },

    onBeginContact: function (contact, self, other) {
        switch(other.tag) {
            case 0:
            case 6:
            break;

            case 7:
            this._audioManager.audioPlay(1);
            this._isBreakDia = true;
            this._dataManager.getDiamond(1);
            this.onBeginContactAction();
            break;

            case 8:
            this._gameScreen.slowMotionStart();
            this.onBeginContactAction();
            break;

            case 9:
            this.node.destroy();
            break;

            case 10:
            this._audioManager.audioPlay(2);
            this.ballCollider.sensor = true;
            this.ballCollider.tag = 9;
            this.ballLocusParticle.active = false;
            this._moveSpeed = 0;
            this._rigidbody.linearVelocity = cc.v2(0,0);
            const particleSetPositionX = this.node.getPositionX();
            const particleSetPositionY = this.node.getPositionY();
            this._particleCreator.particleCreate(2, particleSetPositionX, particleSetPositionY);
            break;

            default:
            if (this.ballLocusParticle.active === false) return;
            if (this._botSensorNode.active === false) {
                this._dataManager.scoreUpdate(1);
            }
            this.onBeginContactAction();
            break;
        }
    },

    onBeginContactAction () {
        if (this._isBreakDia === false) {
            this._audioManager.audioPlay(2);
        }
        const particleSetPositionX = this.node.getPositionX();
        const particleSetPositionY = this.node.getPositionY();
        this._particleCreator.particleCreate(0, particleSetPositionX, particleSetPositionY);
        this._particleCreator.particleCreate(1, particleSetPositionX, particleSetPositionY);
        this.node.destroy();
    }
});
