const ObjectCreator = require("ObjectCreator");

cc.Class({
    extends: cc.Component,

    properties: {
        objectCreator: ObjectCreator,

        gameScreen: cc.Node,
        resultScreen: cc.Node,
        matchingResultScreen: cc.Node,
        objectContainerNode: cc.Node,
        ballContainerNode: cc.Node,

        _particleProductionTimer: 4,
        _isGameOver: false,
        
        _isMatching: false,
    },

    // LIFE-CYCLE CALLBACKS:

    update (dt) {
        if (this._isGameOver === false) return;
        this._particleProductionTimer -= dt;

        if (this._particleProductionTimer < 0) {
            if (this._isMatching === true) {
                this.matchingResultScreen.active = true;
            }
            else {
                this.resultScreen.active = true;
            }
            this.gameScreen.active = false;
            this.objectContainerNode.removeAllChildren();
            this.ballContainerNode.removeAllChildren();
        }
    },

    isGameOver () {
        this._isGameOver = true;
        this.objectCreator.unObjectCreate();
    },

    switchingisMatching (flg) {
        this._isMatching = flg;
    }
});
