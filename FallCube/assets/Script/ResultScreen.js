const DataManager = require("DataManager");
const AudioManager = require("AudioManager");
const FbAdvertising = require("FbAdvertising");
const LeaderBoard = require("Leaderboard");

cc.Class({
    extends: cc.Component,

    properties: {
        dataManager: DataManager,
        audioManager: AudioManager,
        fbAdvertising: FbAdvertising,
        leaderBoard: LeaderBoard,

        retryButtonNode: cc.Node,
        rankingNode: cc.Node,
        buttonNode: cc.Node,

        movieButtonNode: cc.Node,

        debugLabel: cc.Label,
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        this.dataManager.scoreUpdate(0);
        if (this.fbAdvertising.adRewardedLoadCheckr() === false) {
            this.movieButtonNode.active = false;
            this.retryButtonNode.setPosition(cc.v2(0,-200));
        }
        this.fbAdvertising.showInterstitialAd();
        this.audioManager.audioPlay(3);
    },

    // update (dt) {},

    retryButtonAction () {
        if (this.fbAdvertising.adRewardedLoadCheckr() === true) {
            this.dataManager.iswatchRewardSwitch(false);
        }
        cc.director.getScheduler().setTimeScale(1.0);
        //シーンを再読込み
        cc.director.loadScene('Game');
    },

    movieButtonAction () {
        this.fbAdvertising.showRewardedAd();
        console.log("PushButton");
        //this.debugLabel.string = "PushButton";
    },

    rankingButtonAction () {
        this.audioManager.audioPlay(3);
        this.leaderBoard.sendScore(this.dataManager.returnBestScore());
        this.rankingNode.active = true;
        this.buttonNode.active = false;
    },

    rewardRetryButtonAction () {
        this.audioManager.audioPlay(3);
        this.dataManager.iswatchRewardSwitch(true);
        cc.director.getScheduler().setTimeScale(1.0);
        //シーンを再読込み
        cc.director.loadScene('Game');
    }
});
