cc.Class({
    extends: cc.Component,

    properties: {
        _particleCreatorNode: null,
        _particleCreator: null,

        _rigidbody: null,
        _moveSpeed: -460,
        _isSlowMotion: false,
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        this._particleCreatorNode = cc.find('Canvas/ScreenCommonNode/ParticleCreatorNode');
        this._particleCreator = this._particleCreatorNode.getComponent('ParticleCreator');

        this._rigidbody = this.node.getComponent(cc.RigidBody);
        this._rigidbody.linearVelocity = cc.v2(0, this._moveSpeed);
    },

    update (dt) {
        if (this._isSlowMotion === false && cc.director.getScheduler().getTimeScale() < 1) {
            this._isSlowMotion = true;
            this._moveSpeed = this._moveSpeed / 2;
            this._rigidbody.linearVelocity = cc.v2(0, this._moveSpeed);
        }
        else if (this._isSlowMotion === true && cc.director.getScheduler().getTimeScale() === 1){
            this._isSlowMotion = false;
            this._moveSpeed = this._moveSpeed * 2;
            this._rigidbody.linearVelocity = cc.v2(0, this._moveSpeed);
        }
    },

    onBeginContact: function (contact, self, other) {
        switch (other.tag) {
            case 0:
            const particleSetPositionX = this.node.getPositionX();
            const particleSetPositionY = this.node.getPositionY();
            this._particleCreator.particleCreate(4, particleSetPositionX, particleSetPositionY);
            this._particleCreator.particleCreate(5, particleSetPositionX, particleSetPositionY);
            this.node.destroy();
            break;

            case 9:
            this.node.destroy();
            break;
        }
    },
});
