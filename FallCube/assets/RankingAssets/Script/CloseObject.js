cc.Class({
    extends: cc.Component,

    properties: {
        target:{
            default: null,
            type: cc.Node
        },

        titleButtonNode: cc.Node,
        resultButtonNode: cc.Node
    },

    Close: function(){
        this.target.active = false;

        this.titleButtonNode.active = true;
        this.resultButtonNode.active = true;
    },
});
